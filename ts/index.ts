import * as plugins from './webclient.plugins.js';

export class CsWebclient {
  webstore: plugins.webstore.WebStore<plugins.csInterfaces.IWebclientSettings>;

  constructor() {
    this.webstore = new plugins.webstore.WebStore<plugins.csInterfaces.IWebclientSettings>({
      dbName: 'consentsoftware',
      storeName: 'webclient',
    });
  }

  public async isCookieLevelSet(): Promise<boolean> {
    const result = await this.webstore.get('acceptedCookieLevels');
    return !!result;
  }

  public async setCookieLevels(
    cookieLevelsArg: plugins.csInterfaces.TCookieLevel[]
  ): Promise<boolean> {
    await this.webstore.set('acceptedCookieLevels', {
      acceptanceTimestamp: Date.now(),
      acceptedCookieLevels: cookieLevelsArg,
    });
    return true;
  }

  public async getCookieLevels(): Promise<
    plugins.csInterfaces.IWebclientSettings['acceptedCookieLevels']
  > {
    const result = await this.webstore.get('acceptedCookieLevels');
    return result?.acceptedCookieLevels;
  }

  public async isCookieLevelStillValid(): Promise<boolean> {
    if (!this.isCookieLevelSet) {
      return false;
    }
    const result = await this.webstore.get('acceptedCookieLevels');
    if (
      result.acceptanceTimestamp <
      Date.now() - plugins.smarttime.getMilliSecondsFromUnits({ months: 3 })
    ) {
      return false;
    }
    return true;
  }

  public async getAndRunConsentTuples(domainArg?: string) {
    const acceptedCookieLevels = await this.getCookieLevels();
    if (!acceptedCookieLevels) {
      console.log('You need to set accepted cookielevels first');
      return;
    }
    const csGetDomainSettingsRequest = new plugins.typedrequest.TypedRequest<
      plugins.csInterfaces.IRequest_Client_ConsentSoftwareServer_GetDomainSettings
    >('https://connect.api.global/consentsoftware', 'getDomainSettings');
    const domainToRequest = domainArg || window.location.hostname;
    const response = await csGetDomainSettingsRequest.fire({
      domain: domainToRequest,
    });
    for (const consentTuple of response.consentTuples) {
      if (
        consentTuple.level === 'functional' ||
        acceptedCookieLevels.includes(consentTuple.level)
      ) {
        const scriptString = consentTuple.script as string;
        // tslint:disable-next-line: function-constructor
        const tupleFunction: plugins.csInterfaces.IConsentTuple['script'] = new Function(
          'dataArg',
          'cookieLevelsArg',
          `return (${scriptString})(dataArg, cookieLevelsArg)`
        ) as plugins.csInterfaces.IConsentTuple['script'];
        if (typeof tupleFunction === 'function') {
          await tupleFunction(consentTuple.scriptExecutionDataArg, await this.getCookieLevels());
        } else {
          const errorText = 'got malformed script to execuute';
          console.error(errorText);
          throw new Error(errorText);
        }
        console.log(
          `Successfully executed ConsentTuple >>${consentTuple.name}<< -> ${consentTuple.description}`
        );
      }
    }
  }
}
